package com.livelo.liveloteste;

import com.livelo.liveloteste.dataprovider.api.response.ConsultaPessoaResponse;
import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import com.livelo.liveloteste.dataprovider.converter.PeopleConverter;
import com.livelo.liveloteste.dataprovider.converter.PeopleConverterImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class PeopleConverterImplTest extends PeopleMock {

    private PeopleConverter converter;

    private ResponseEntity<ConsultaPessoaResponse> consultaPessoaResponse;

    @Before
    public void setup (){
        converter = new PeopleConverterImpl();
    }

    /*
      metodos pendentes de implementação...

    */
    @Test
    public void shouldTestConverterResponseToListPeopleNull(){
        ResponseEntity<ConsultaPessoaResponse> response = new ResponseEntity<ConsultaPessoaResponse>(HttpStatus.OK);
    }

    @Test
    public void shouldTestConverterResponsePeople(){
        ResponseEntity<PeopleResponse> response = new ResponseEntity<PeopleResponse>(HttpStatus.OK);
    }

    @Test
    public void shouldTestConverterResponsePeopleNull(){
        ResponseEntity<PeopleResponse> response = new ResponseEntity<PeopleResponse>(HttpStatus.OK);
    }

}
