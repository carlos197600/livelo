package com.livelo.liveloteste;

import com.livelo.liveloteste.dataprovider.api.response.ConsultaPessoaResponse;
import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
class PeopleIntegrationTests {

    static final String baseURLInexistente = "https://swapi.dev/api/peopl";
    static final String baseURL = "https://swapi.dev/api/people";
    static final Long id = 1L;
    static final Long idInexistente = 1000L;
    static RestTemplate restTemplate = new RestTemplateBuilder().rootUri(baseURL).build();
    static RestTemplate restTemplate2 = new RestTemplateBuilder().rootUri(baseURLInexistente).build();

    @Test
    public void shouldTestGetPeople_whenGetPeople_thenIsNull() {
        ResponseEntity<PeopleResponse> result = restTemplate.getForEntity("/{id}", PeopleResponse.class, id);
        Assertions.assertNotNull(result.getBody());
    }

    @Test
    public void shouldTestGetPeople_whenGetPeople_thenIsNotFound() {
        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> {
            ResponseEntity<PeopleResponse> result = restTemplate.getForEntity("/{id}", PeopleResponse.class, idInexistente);
        });
        Assertions.assertNotNull(exception);
    }

    @Test
    public void shouldGetPeople_whenGetPeople_thenIsNotFound2() {
        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> {
            ResponseEntity<PeopleResponse> result = restTemplate2.getForEntity("/{id}", PeopleResponse.class, id);
        });
        Assertions.assertNotNull(exception);
    }

    @Test
    public void shouldGetPeople_whenGetPeopleAll_thenIsNotNull() {
        ResponseEntity<String> result = restTemplate.getForEntity("/", String.class);
        Assertions.assertNotNull(result.getBody());
    }

    @Test
    public void shouldGetPeople_whenGetPeopleAll_thenIsNotFound() {
        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> {
            ResponseEntity<ConsultaPessoaResponse> result = restTemplate2.getForEntity("/", ConsultaPessoaResponse.class, id);
        });
        Assertions.assertNotNull(exception);
    }

    @Test
    void contextLoads() {
    }
}
