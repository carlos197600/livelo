package com.livelo.liveloteste;

import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import com.livelo.liveloteste.dataprovider.entity.People;

import java.util.ArrayList;
import java.util.List;

public class PeopleMock {

    public People getPeople(){
        return People.builder().name("Joao da Silva")
                .gender("M")
                .build();
    }

    public List<People> getPeopleAll(){
        List<People> peoples = new ArrayList<>();
        peoples.add(new People("Joao da Silva","","","","",""));
        peoples.add(new People("Maria da Silva","","","","",""));
        peoples.add(new People("CPU Invocada","","","","",""));
        return peoples;
    }

    public List<PeopleResponse> getPeopleAllResponse(){
        List<PeopleResponse> peoples = new ArrayList<>();
        peoples.add(new PeopleResponse("Joao da Silva","","","","",""));
        peoples.add(new PeopleResponse("Maria da Silva","","","","",""));
        peoples.add(new PeopleResponse("CPU Invocada","","","","",""));
        return peoples;
    }
}
