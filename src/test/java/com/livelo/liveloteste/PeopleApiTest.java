package com.livelo.liveloteste;

import com.livelo.liveloteste.dataprovider.api.PeopleApi;
import com.livelo.liveloteste.dataprovider.entity.People;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PeopleApiTest extends PeopleMock{

    @Mock
    private PeopleApi api;

    @Test
    public void givenPeople_whenGetPeople_thenIsNull(){
        Mockito.when(api.obterPeoplesByID(1L)).thenReturn(null);
        People retorno = api.obterPeoplesByID(1L);
        Assertions.assertEquals(null,retorno);
    }

    @Test
    public void givenPeople_whenGetPeople_thenIsNotNull(){
        Mockito.when(api.obterPeoplesByID(1L)).thenReturn(getPeople());
        People retorno = api.obterPeoplesByID(1L);
        Assertions.assertEquals("Joao da Silva",retorno.getName());
        Assertions.assertEquals("M",retorno.getGender());
    }

    @Test
    public void givenPeopleAll_whenGetPeopleAll_thenIsNull(){
        Mockito.when(api.obterPeoplesAll()).thenReturn(null);
        List<People> retorno = api.obterPeoplesAll();
        Assertions.assertEquals(null,retorno);
    }

    @Test
    public void givenPeopleAll_whenGetPeopleAll_thenIsNotNull(){
        Mockito.when(api.obterPeoplesAll()).thenReturn(getPeopleAll());
        List<People> retorno = api.obterPeoplesAll();
        Assertions.assertEquals(3,retorno.size());
    }
}
