package com.livelo.liveloteste;

import com.livelo.liveloteste.entrypoint.rest.PeopleRestEntryPoint;
import com.livelo.liveloteste.usecase.PeopleUseCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PeopleRestEntryPoint.class)
public class PeopleRestEntryPointTest extends PeopleMock{

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PeopleUseCase useCase;

    @Test
    public void givenPeople_whenGetPeople_thenStatus200() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .get("/api/people/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void givenPeople_whenGetPeople_thenStatus404() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .get("/people/api/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void givenPeople_whenGetPeopleUseCase_thenStatus200() throws Exception {
        Mockito.when(useCase.obterPeopleById(1L)).thenReturn(getPeople());
        mvc.perform( MockMvcRequestBuilders
                .get("/api/people/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status()
                .isOk());
    }

}
