package com.livelo.liveloteste.usecase;

import com.livelo.liveloteste.dataprovider.entity.People;

import java.util.List;

public interface PeopleUseCase {
    public People obterPeopleById(Long id);

    public List<People> obterPeopleAll();
}
