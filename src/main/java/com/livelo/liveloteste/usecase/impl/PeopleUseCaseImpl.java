package com.livelo.liveloteste.usecase.impl;

import com.livelo.liveloteste.dataprovider.api.PeopleApi;
import com.livelo.liveloteste.dataprovider.entity.People;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import com.livelo.liveloteste.usecase.PeopleUseCase;
import java.util.List;

@RequiredArgsConstructor
@Component
@Log4j2
public class PeopleUseCaseImpl implements PeopleUseCase {

    private final PeopleApi api;

    @Override
    public People obterPeopleById(Long id) {
        return api.obterPeoplesByID(id);
    }

    @Override
    public List<People> obterPeopleAll() {
        return api.obterPeoplesAll();
    }
}


