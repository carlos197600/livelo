package com.livelo.liveloteste.entrypoint;

import com.livelo.liveloteste.dataprovider.entity.People;
import org.springframework.http.ResponseEntity;

public interface PeopleEntryPoint {

    public ResponseEntity<People> obterPeopleById(Long id);

}
