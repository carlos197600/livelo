package com.livelo.liveloteste.entrypoint.rest;

import com.livelo.liveloteste.dataprovider.entity.People;
import com.livelo.liveloteste.entrypoint.PeopleEntryPoint;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.livelo.liveloteste.usecase.PeopleUseCase;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
@Log4j2
public class PeopleRestEntryPoint implements PeopleEntryPoint {

    private final PeopleUseCase peopleUseCase ;

    @Override
    @GetMapping("people/{id}")
    public ResponseEntity<People> obterPeopleById(@PathVariable Long id) {
        log.info("Inicio chamada para consulta People {}", id);
        try {
            return new ResponseEntity<>(peopleUseCase.obterPeopleById(id), HttpStatus.OK);
        } catch (IllegalArgumentException ie) {
            log.error("Parametros invalidos ", ie.getMessage(), ie);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        } catch (HttpClientErrorException.NotFound ne) {
            log.error("Recurso nao encontardo ", ne.getMessage(), ne);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            //Para um tratamento adequado de Excecoes pode ser utilizado
            //um mecanimo para interceptar os erros. Ex;
            //@ControllerAdvice
            //public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler
            //Anota o metodo com os erros especialista @ExceptionHandler(value = {IllegalArgumentException.class})
            log.error("Erro inesparado ", e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}



