package com.livelo.liveloteste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LivelotesteApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivelotesteApplication.class, args);
	}

}
