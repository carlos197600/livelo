package com.livelo.liveloteste.dataprovider.converter;

import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import com.livelo.liveloteste.dataprovider.entity.People;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PeopleConverter {
    public People converterResponseToPeople(ResponseEntity<PeopleResponse> response);

    public List<People> converterResponseToListPeople(String response);
}
