package com.livelo.liveloteste.dataprovider.converter;


import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import com.livelo.liveloteste.dataprovider.entity.People;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Component
public class PeopleConverterImpl implements PeopleConverter {

    public People converterResponseToPeople(ResponseEntity<PeopleResponse> response) {
        if (response == null && response.getBody() == null) {
            return null; // disparar erro
        }
        return People.builder().name(response.getBody().getName())
                .gender(response.getBody().getGender())
                .height(response.getBody().getHeight())
                .mass(response.getBody().getMass())
                .hairColor(response.getBody().getHairColor())
                .build();
    }

    public List<People> converterResponseToListPeople(String response) {
        List<People> peoples = Collections.emptyList() ;
        return peoples;
    }

    public People consultarPeople(PeopleResponse response){
        return People.builder().name(response.getName())
                .gender(response.getGender())
                .height(response.getHeight())
                .mass(response.getMass())
                .hairColor(response.getHairColor())
                .build();
    }
}
