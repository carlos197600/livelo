package com.livelo.liveloteste.dataprovider.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@ToString
public class People {
    private String name;
    private String height;
    private String mass;
    private String hairColor;
    private String skinColor;
    private String gender;

}

