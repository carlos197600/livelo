package com.livelo.liveloteste.dataprovider.api;

import com.livelo.liveloteste.dataprovider.entity.People;

import java.util.List;

public interface PeopleApi {
    public People obterPeoplesByID(Long id) ;
    public List<People> obterPeoplesAll();
}
