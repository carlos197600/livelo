package com.livelo.liveloteste.dataprovider.api.response;

import lombok.Data;
import java.util.List;

@Data
public class ConsultaPessoaResponse {
    private List<PeopleResponse> peoples;
}
