package com.livelo.liveloteste.dataprovider.api.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PeopleResponse {
    private String name;
    private String height;
    private String mass;
    private String hairColor;
    private String skinColor;
    private String gender;

}

