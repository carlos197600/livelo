package com.livelo.liveloteste.dataprovider.impl;

import com.livelo.liveloteste.dataprovider.api.PeopleApi;
import com.livelo.liveloteste.dataprovider.api.response.ConsultaPessoaResponse;
import com.livelo.liveloteste.dataprovider.converter.PeopleConverter;
import com.livelo.liveloteste.dataprovider.entity.People;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import com.livelo.liveloteste.dataprovider.api.response.PeopleResponse;
import java.util.List;

@RequiredArgsConstructor
@Component
@Log4j2
public class PeopleApiImpl implements PeopleApi {

    private final PeopleConverter converter;

    //Em uma aplicacao real , esses dados sao levados para arquivos de config
    static final String baseURL = "https://swapi.dev/api/people";

    // Em uma aplicacao real , esse objeto será injetado pelo Spring.
    RestTemplate restTemplate = new RestTemplateBuilder().rootUri(baseURL).build();

    @Override
    public People obterPeoplesByID(Long id) {
        People people = null;
        try {
            ResponseEntity<PeopleResponse> response = restTemplate.getForEntity("/{id}", PeopleResponse.class, id);
            log.info("Response da API Aberta", response.getStatusCode());
            people = converter.converterResponseToPeople(response);
        } catch (HttpServerErrorException e) {
            log.error("Erro na chamada do endpoint people ", e.getMessage(), e);
        }
        return people != null ? people : People.builder().build();
    }

    @Override
    public List<People> obterPeoplesAll() {
        ResponseEntity<String> result = restTemplate.getForEntity("/", String.class);
        return converter.converterResponseToListPeople(result.toString());
    }
}
